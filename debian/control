Source: forked-daapd
Maintainer: Debian Multimedia Maintainers <debian-multimedia@lists.debian.org>
Section: sound
Priority: optional
Build-Depends: debhelper-compat (= 13),
               libavcodec-dev,
               libavformat-dev,
               libswscale-dev,
               libavutil-dev,
               libavfilter-dev,
               libinotify-dev [kfreebsd-any],
               libavahi-client-dev,
               libsodium-dev,
               libsqlite3-dev,
               libconfuse-dev,
               libmxml-dev,
               libplist-dev,
               libpulse-dev,
               libgcrypt20-dev,
               libgpg-error-dev,
               libjson-c-dev,
               libprotobuf-c-dev,
               libgnutls28-dev,
               libasound2-dev,
               libwebsockets-dev,
               zlib1g-dev,
               libunistring-dev,
               libtre-dev,
               libevent-dev,
               libantlr3c-dev,
               antlr3,
               gperf,
               libcurl4-gnutls-dev
Standards-Version: 4.6.1
Rules-Requires-Root: no
Vcs-Browser: https://salsa.debian.org/multimedia-team/forked-daapd
Vcs-Git: https://salsa.debian.org/multimedia-team/forked-daapd.git
Homepage: https://github.com/ejurgensen/forked-daapd

Package: forked-daapd
Architecture: any
Pre-Depends: ${misc:Pre-Depends}
Depends: ${shlibs:Depends},
         ${misc:Depends},
         avahi-daemon (>= 0.6.31-3~),
         adduser,
         lsb-base (>= 3.0-6),
         psmisc
Recommends: libavcodec-extra
Description: DAAP/DACP (iTunes) server, support for AirPlay and Roku devices
 forked-daapd is an iTunes-compatible media server, originally intended
 as a rewrite of Firefly Media Server (also known as mt-daapd).
 .
 It supports a wide range of audio formats, can stream video to iTunes,
 XBMC and other compatible clients, has support for Apple's Remote
 iPhone/iPod application and can stream music to AirPlay devices like
 the AirPort Express.
 .
 It also features RSP support for Roku's SoundBridge devices and MPD support for
 Music Player Daemon clients.
 .
 Built-in, on-the-fly decoding support enables serving popular free music
 formats like FLAC, Ogg Vorbis or Musepack to those clients that do not
 otherwise support them.
